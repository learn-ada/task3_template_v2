with Window;
with Screen;
with Player;
with Image;

procedure task3 is
   bm_im : Image.Bitmap := Image.Load_Bitmap("resources/texture1.bmp");
begin

   --test image reading
   for y in bm_im'First(2) .. bm_im'Last(2)/2 loop
    for x in bm_im'First(1) .. bm_im'Last(1)/2 loop
         bm_im(x, y) := (0, 255, 0, 0);
    end loop;
   end loop;
   Image.Save_Bitmap(bm_im, "resources/texture2.png");

   Window.Init;

   Window.Open_Window (Width => Screen.g_width, Height => Screen.g_height);

   Screen.Fill_Screen (Screen.background_color);
   while not Window.Escape_Pressed and
         Window.Window_Opened loop
      Player.ProcessInput;
      Player.Draw;
      Screen.Draw;

      Window.Swap_Buffers;

      Window.Poll_Events;
   end loop;

   Window.Shutdown;

end task3;
