with Window;

package body Player is

  procedure Draw is
  begin
    if PlayerMoved then
      for i in old_coords.x_min .. old_coords.x_max loop
        for j in old_coords.y_min .. old_coords.y_max loop
          Screen.Put_Pixel(x => i, y => j, pix => Screen.background_color);
        end loop;
      end loop;
    end if;

    for i in coords.x_min .. coords.x_max loop
      for j in coords.y_min .. coords.y_max loop
        Screen.Put_Pixel(x => i, y => j, pix => player_color);
      end loop;
    end loop;

  end Draw;

  procedure ProcessInput is
  begin
    old_coords := coords;
    if Window.Up_Pressed then
      coords.y_min := coords.y_min - move_speed;
      coords.y_max := coords.y_max - move_speed;
    elsif Window.Down_Pressed then
      coords.y_min := coords.y_min + move_speed;
      coords.y_max := coords.y_max + move_speed;
    end if;
    if Window.Left_Pressed then
      coords.x_min := coords.x_min - move_speed;
      coords.x_max := coords.x_max - move_speed;
    elsif Window.Right_Pressed then
      coords.x_min := coords.x_min + move_speed;
      coords.x_max := coords.x_max + move_speed;
    end if;
  end ProcessInput;

  function PlayerMoved return Boolean is
  begin
    if coords.x_min = old_coords.x_min and coords.y_min = old_coords.y_min then
      return False;
    else
      return True;
    end if;

  end PlayerMoved;


end Player;
