with Screen;

package Player is

  type coords_player_t is record
    x_min, y_min : Integer;
    x_max, y_max : Integer;
  end record;

  coords: coords_player_t := (x_min => 254, y_min => 254,
                              x_max => 256, y_max => 256);

  old_coords: coords_player_t := (x_min => 254, y_min => 254,
                              x_max => 256, y_max => 256);

  player_color: Screen.pixel := (255, 255, 0, 0);

  move_speed: Integer := 1;

  procedure ProcessInput;
  procedure Draw;

  function PlayerMoved return Boolean;

end Player;
